var path = require('path');

require('./').run({
    migrationsDir: path.resolve(__dirname, 'migrations'),
    user: 'dbmigrate_test',
    host: 'localhost',
    db: 'dbmigrate_test',
    password: 'dbmigrate_test',
    port: 5435
});
